/*
3. Faça um algoritmo em que você recebe 3 notas de um aluno e caso a
média aritmética dessas notas for maior ou igual que 6 imprima
“Aprovado”, caso contrário “Reprovado”.
*/

function testaAluno(n1,n2,n3){
    let media = (n1+n2+n3)/3
    if (media >= 6){
        console.log('Arovado')
    }else{
        console.log('Reprovado')
    }
    return media
}

/* Declaração de variáveis*/

let nota1
let nota2
let nota3
let media

/* Teste 1 */
console.log('Aluno 1:')
nota1 = 10
nota2 = 3
nota3 = 7
media = testaAluno(nota1,nota2,nota3)
console.log('Media = ',media,'\n')

/* Teste 2 */
console.log('Aluno 2:')
nota1 = 6
nota2 = 5
nota3 = 6
media = testaAluno(nota1,nota2,nota3)
console.log('Media = ',media,'\n')
