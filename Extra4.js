/*
4. Crie um algoritmo que transforma as notas do sistema numérico para
sistema de notas em caracteres Tipo A, B e C
● hde 90 para cima - A
● entre 80 e 90 -B
● entre 70 e 79 - C
● entre 60 e 69 - D
● menor que 60 - F
*/

let result
for (let notai = -5; notai <= 105; notai += 5){
    console.log(notai)
    result = transfNota(notai)
    console.log(result + '\n')
}

function transfNota(nota){
    if (nota > 100 || nota < 0){
        return 'inválido'
    }
    if (nota >= 90){
        return 'A'
    }
    if (nota >= 80){
        return 'B'
    }
    if (nota >= 70){
        return 'C'
    }
    if (nota >= 60){
        return 'D'
    }
    if (nota < 60){
        return 'F'
    }
}