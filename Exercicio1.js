/*
1. Implemente um algoritmo que pegue duas matrizes (array de arrays) e
realize sua multiplicação. Lembrando que para realizar a multiplicação
dessas matrizes o número de colunas da primeira matriz tem que ser
igual ao número de linhas da segunda matriz. (2x2)
a. Caso teste 1 : [ [ [2],[-1] ], [ [2],[0] ] ] e [ [2,3],[-2,1] ] multiplicadas dão
[ [6,5], [4,6] ]
b. Caso teste 2 : [ [4,0], [-1,-1] ] e [ [-1,3], [2,7] ] multiplicadas dão [
    [-4,12], [-1,-10] ]
*/

/* Construção das matrizes */

let matriz1 = [ [2,-1] , [2,0] ]
let matriz2 = [ [2,3],[-2,1] ]


/* IMPRESSÃO DA MATRIZ 1 NA TELA */
/*
console.log('\n Matriz 1 \n')
let numlinha = 0
matriz1.forEach(linha => {
    console.log('linha ',numlinha)
    numlinha++
    linha.forEach(elemento => {
        console.log('elemento ',elemento )
    });
});
console.log('')
*/

function produtoVetorial(matriz1,matriz2){
    if (matriz1[0].length == matriz2.length){
        let matriz3 = []
        let linham3 = []
        var soma
        for(let i=0; i<matriz1.length; i++){
            for(let j=0; j<matriz2[0].length; j++){
                soma = 0
                for(let k=0; k<matriz1[0].length; k++){
                    soma = soma + matriz1[i][k] * matriz2[k][j]
                }
                linham3.push(soma)
            }
            matriz3.push(linham3)
            linham3 = []
        }
        return matriz3
    } else{
        console.log("O número de colunas da primeira matriz deve ser igual ao número de linhas da segunda matriz para realizar a operação.")
    }
    return [0]
}

 /* Teste 1 */

var resultado1 = produtoVetorial(matriz1,matriz2)
resultado1.forEach(linha => {
    console.log(linha)
})
console.log('')


/* Teste 2 */

matriz1 = [ [4,0], [-1,-1] ] 
matriz2 = [ [-1,3], [2,7] ]

var resultado2 = produtoVetorial(matriz1,matriz2)
resultado2.forEach(linha =>{
    console.log(linha)
})
console.log('')

/* Teste 3 */

matriz1 = [ [4,0,0], [-1,-1,0] ] 
matriz2 = [ [-1,3], [2,7] ]

var resultado3 = produtoVetorial(matriz1,matriz2)
resultado3.forEach(linha =>{
    console.log(linha)
})
console.log('')