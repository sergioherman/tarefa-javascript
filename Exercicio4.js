/*
4. Vocês receberão um arquivo com um array de objetos representando
deuses do jogo Smite. Usando os métodos aprendidos em aula, faça os
seguintes exercícios:
● Q1. Imprima o nome e a quantidade de features de todos os deuses usando
uma única linha de código.
● Q2. Imprima todos os deuses que possuem o papel de "Mid"
● Q3. Organize a lista pelo panteão do deus.
● Q4. Faça um código que retorne um novo array com o nome de cada deus e
entre parênteses, a sua classe.
Por exemplo, o array deverá ficar assim: ["Achilles (Warrior)", "Agni (Mage)", ...]
*/


const gods = require("./arquivo_exercicio_4");

/* Q1 */
for (k in gods){console.log(gods[k].name,gods[k].features.length)}

/* Q2 */

let nn=0
for (k in gods){
  if (gods[k].roles == 'Mid'){
    console.log(nn,JSON.stringify(gods[k]))    
    nn++
  }
}


/* Q3 */

let trocou = true
temp = [{}]
while (trocou){
  trocou = false
  for (let k = 0; k < (gods.length - 1); k++){
    if (gods[k].pantheon > gods[k+1].pantheon){
      temp = gods[k]
      gods[k] = gods[k+1]
      gods[k+1] = temp
      trocou = true
    }
  }
}
console.log(gods)


/* Q4 */

function montaVetor(deuses){
  let vetDeuses = []
  for(k in gods){
    let nomeDeus = String(deuses[k].name)
    let classeDeus = String(deuses[k].class)
    vetDeuses.push(nomeDeus + ' ('+ classeDeus + ')')
  }
  return vetDeuses
}

let n = 0
let vetor = montaVetor(gods)
vetor.forEach(deus => {
  console.log(n+': "'+deus+'"')
  n++
});



