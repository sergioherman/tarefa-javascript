/*
Escreva um algoritmo para ler uma temperatura em graus Fahrenheit,
calcular e escrever o valor correspondente em graus Celsius (baseado na
fórmula abaixo):
𝐶/5 = ( 𝑓 − 32)/9
*/

let tempF = 95

function convFC(tempFar){
    let tempCel = 5 * (tempFar - 32 ) / 9
    return tempCel
}

let tempC = convFC(tempF)
console.log('A temperatura ',tempF,'ºF convertida para em graus Celsius é: ', tempC,'ºC')