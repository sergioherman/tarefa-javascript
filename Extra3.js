/*
3. Escolha 5 valores inteiros e ordene-os em ordem crescente. No final,
mostre os valores em ordem crescente, uma linha em branco e em
seguida, os valores na sequência
*/

var num
var vetNum = []

for (let i = 0; i<6; i++){
    num = Math.floor(Math.random() * 200 - 100)
    vetNum.push(num)
}

let continuar = true
while (continuar == true){
    continuar = false
    for(let i = 0; i < (vetNum.length - 1); i++){
        if (vetNum[i] > vetNum[i+1]){
            temp = vetNum[i]
            vetNum[i] = vetNum[i+1]
            vetNum[i+1] = temp
            continuar = true
        }
    }
}

vetNum.forEach(element => {
    console.log(element)
});

console.log('')

let textNum = ''

for (numero in vetNum){
    textNum += vetNum[numero]
    if (numero < vetNum.length - 1)
    textNum += ' '
}
console.log(textNum)