/*
1. Faça um programa que leia 6 valores. Estes valores são somente
negativos ou positivos (desconsidere os valores nulos). A seguir, mostre a
quantidade de valores positivos digitados
*/

let k = 0
for (let i = 0; i<6; i++){
    let num = Math.floor(Math.random() * 200 - 100)
    console.log(num)
    k += testePositivo(num)
}
console.log('\nHá '+k+' números positivos.')

function testePositivo(num) {
    if (num >= 0){
        return 1
    }else{
        return 0
    }
}