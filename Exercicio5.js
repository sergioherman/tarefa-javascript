/*
5. Teste 5 números inteiros aleatórios. Os testes:
● Caso o valor seja divisível por 3, imprima no console “fizz”.
● Caso o valor seja divisível por 5 imprima “buzz”.
● Caso o valor seja divisível por 3 e 5, ao mesmo tempo, imprima
“fizzbuzz”.
● Caso contrário imprima nada.
*/

function testeFizzBuzz(num){
    let texto = ''
    if (num % 3 == 0){
        texto = 'fizz'
    }
    if (num % 5 == 0){
        texto += 'buzz'
    }
    if (texto != ''){
        console.log(texto)
    }
}

for (let i = 0; i<5; i++){
    let num = Math.floor(Math.random() * 100)
    console.log(num)
    testeFizzBuzz(num)
    console.log('')
}
