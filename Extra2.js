/*
2. Implemente uma função recursiva que,dados dois números inteiros x e
n, calcula o valor de xn.
*/

x = 5
n = 7

let result = retornaX(x,n)
console.log(result)

function retornaX(x,n){
    n -= 1
    if (n>0){
        x += retornaX(x,n)
    }
    return x
}